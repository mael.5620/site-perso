import { createClient } from 'contentful'

const types = ['homePage', 'projets', 'realisations', 'services']

const client = require('contentful').createClient({
    space: process.env.NEXT_PUBLIC_CONTENTFUL_SPACE_ID,
    accessToken: process.env.NEXT_PUBLIC_CONTENTFUL_ACCESS_TOKEN
})

const orderServices = (a, b) => {

    const serviceA = a.fields.order
    const serviceB = b.fields.order

    let comparaison = 0
    if (serviceA > serviceB) {
        comparaison = 1
    } else {
        comparaison = -1
    }
    return comparaison
}

const orderProjects = (a, b) => {

    const projectA = a.fields.order
    const projectB = b.fields.order

    let comparaison = 0
    if (projectA > projectB) {
        comparaison = -1
    } else {
        comparaison = 1
    }
    return comparaison
}

export async function fetchEntries() {
    const data = {}

    for (const type of types) {
        const entries = await client.getEntries({content_type: type})
        if (entries.items) {
            data[type] = entries.items
            if (type == 'services') {
                data.services = entries.items.sort(orderServices)
            } else if (type == 'projets' || type == 'realisations') {
                data[type] = entries.items.sort(orderProjects)
            }
        } else {
            console.log(`Error getting entries for ${contentType.name}`)
        }
    }

    return data
}