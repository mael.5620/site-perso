import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html>
        <Head>
            <title>Maël Larcher | Portfolio</title>
            <meta name="description" content="Je m’appelle Maël, je suis étudiant en BTS SIO option SLAM. Passionné par le développement informatique je réalise divers projets personnels afin de développer mes compétences."/>
            <meta name="google-site-verification" content="hK_gTQ87n13napT-bMvf260xcN2rTfTlCCTeSEGMxO8" />
            <meta name="viewport" content="initial-scale=1.0, width=device-width" key="viewport" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument