import { useEffect, useState } from 'react'
import styles from '../styles/Home.module.css'
import {fetchEntries} from '../helpers/getContent'
import Hero from '../components/Hero/Hero';
import AboutMe from '../components/AboutMe/AboutMe';
import ProjectsGrid from '../components/ProjectsGrid/ProjectsGrid';
import Footer from '../components/Footer/Footer'
import Contact from '../components/Contact/Contact';
import MyServices from '../components/MyServices/MyServices';

const Home = (props) => {

  const [contactSubject, setContactSubject] = useState(false)

  const {homePage, projets, realisations, services} = props
  return (
    <div>
      <Hero imageUrl={homePage[0].fields.heroImage.fields.file.url} />
      <AboutMe 
        profileImage={homePage[0].fields.profileImage.fields.file.url} 
        description={homePage[0].fields.description}
      />
      {
        realisations[0] ?
          <ProjectsGrid projects={realisations} bg="#e8e8e8" title="Réalisations" description={homePage[0].fields.realisationsDesc} /> :
          null
      }
      <ProjectsGrid projects={projets} bg={realisations[0] ? null : "#e8e8e8"} title="Projets personnels" description={homePage[0].fields.ProjetPersoDesc} />
      <MyServices setContactSubject={setContactSubject} services={services} description={homePage[0].fields.servicesDesc} />
      <Contact contactSubject={contactSubject} />
      <Footer />
    </div>
  );
};

export default Home;

export async function getServerSideProps(context) {
  const data = await fetchEntries()

  return {
    props: {...data}
  }
}