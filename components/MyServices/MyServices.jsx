import styles from './MyServices.module.css'

const MyServices = ({ services, description, setContactSubject }) => {
    return (
        <section className={styles.section} >
            <h2 className={styles.title} >Mes services</h2>
            <p className={styles.description} >{ description }</p>
            <div className={styles.container} >
                {
                    services.map(service => (
                        <figure className={styles.service_card} >
                            <h4 className={styles.service_name}> { service.fields.title } </h4>
                            <img className={styles.image} src={service.fields.image.fields.file.url} alt=""/>
                            <figcaption className={styles.caption} >
                                <p className={styles.service_description} > {service.fields.description} </p>
                                <button onClick={() => setContactSubject(service.fields.title)} className={styles.button} ><a href="#contact">Plus de renseignements</a></button>
                            </figcaption>
                        </figure>
                    ))
                }
            </div>
        </section>
    );
};

export default MyServices;