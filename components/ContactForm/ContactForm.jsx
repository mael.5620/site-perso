import { Component, useEffect } from 'react'
import styles from './ContactForm.module.css'

class ContactForm extends Component {
  constructor(props) {
    super(props);
    this.submitForm = this.submitForm.bind(this);
    this.state = {
      status: "",
      name: "",
      email: "",
      subject: "",
      message: ""
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.contactSubject !== this.props.contactSubject) {
      this.setState({subject: this.props.contactSubject})
    }
  }
  
  handleChange(index, value) {
    this.setState({
      [index]: value
    })
  }

  submitForm(ev) {
  ev.preventDefault();
  const form = ev.target;
  const data = new FormData(form);
  const xhr = new XMLHttpRequest();
  xhr.open(form.method, form.action);
  xhr.setRequestHeader("Accept", "application/json");
  xhr.onreadystatechange = () => {
      if (xhr.readyState !== XMLHttpRequest.DONE) return;
      if (xhr.status === 200) {
      form.reset();
      this.setState({ status: "SUCCESS" });
      } else {
      this.setState({ status: "ERROR" });
      }
  };
  xhr.send(data);
  }

  render() {
      const { status, name, email, subject, message } = this.state;
      const { contactSubject } = this.props
      return (
        <div className={styles.container} >
          <form
            className={styles.form}
            onSubmit={this.submitForm}
            action="https://formspree.io/f/mnqoveze"
            method="POST"
            id="contactForm"
            >
              <input className={styles.input} type="text" name="Nom" placeholder="Nom et Prénom :"  onChange={e => this.handleChange("name", e.target.value)}/>
              <input className={styles.input} type="email" name="Email" placeholder="Email :"  onChange={e => this.handleChange("email", e.target.value)}/>
              <select className={styles.input} type="select" name="Objet de la demande" defaultValue="default" value={subject ? subject : "default"} onChange={e => this.handleChange("subject", e.target.value)}>
                <option value="default" disabled >Objet de votre demande :</option>
                <option value="Création d'un site vitrine">Création d'un site vitrine</option>
                <option value="Création d'un site de vente en ligne">Création d'un site de vente en ligne</option>
                <option value="Création d'une page Facebook">Création d'une page Facebook</option>
                <option value="Autre">Autre</option>
              </select>
              <textarea rows={10} className={styles.input} type="text" name="Message" placeholder="Votre message :" onChange={e => this.handleChange("message", e.target.value)}/>
              
              {
                status === "SUCCESS" ? 
                <p className={styles.success} >Merci pour votre message ! </p> : 
                (
                  name && email && subject && message != "" ? 
                  <button className={styles.submit} >Envoyer</button> : 
                  <button className={styles.submit_disabled} disabled >Envoyer</button>
                )
              }
              {status === "ERROR" && <p className={styles.error} >Ooops! Il y a eu une erreur.</p>}
          </form>
        </div>
      );
  }
}

export default ContactForm;