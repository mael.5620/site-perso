import { useState } from 'react';
import styles from './ProjectsGrid.module.css'

const ProjectsGrid = ({ projects, title, bg = "", description }) => {
    const [slice, setSlice] = useState(3);
    return (
        <section className={styles.section} style={{backgroundColor: bg}} >
            <h2 className={styles.title} >{ title }</h2>
            <p className={styles.description} >{ description }</p>
            <div className={styles.container} >
                {
                    projects.slice(0, slice).map(item => (
                        <figure className={styles.project_card} >
                            <img className={styles.image}  src={item.fields.image.fields.file.url} alt=""/>
                            <figureCaption className={styles.text_container} >
                                <h4 className={styles.project_name} > {item.fields.title} </h4>
                                <p className={styles.card_description} > {item.fields.description} </p>
                                <button className={styles.button} ><a className={styles.link}  href={item.fields.url} target="_blank">Visiter le site</a></button>
                            </figureCaption>
                        </figure>
                    ))
                }
            </div>
            {
                projects.length > 3 ? (
                    slice == 3 ?
                        <button className={styles.display_more} onClick={() => setSlice(projects.length)} >Afficher plus</button> :
                        <button className={styles.display_more} onClick={() => setSlice(3)} >Afficher moins</button>
                ) : null
            }
        </section>
    );
};

export default ProjectsGrid;