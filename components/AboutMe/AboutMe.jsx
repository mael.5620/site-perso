import React from 'react';
import styles from './AboutMe.module.css'

const AboutMe = ({profileImage, description}) => {
    return (
        <div className={styles.container} id="about" >
            <img className={styles.image} src={profileImage} alt=""/>
            <h2 className={styles.title} >Qui suis je ?</h2>
            <p className={styles.description} > {description} </p>
        </div>
    );
};

export default AboutMe;