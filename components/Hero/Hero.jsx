import { urlObjectKeys } from 'next/dist/next-server/lib/utils';
import React from 'react'
import styles from './Hero.module.css'

const Hero = ({imageUrl}) => {
    return (
        <div className={styles.container} style={{
            backgroundImage: `url(${imageUrl})`
        }} >
            <div className={styles.sub_container} >
                <h1 className={styles.title} >Maël Larcher</h1>
                <h5 className={styles.sub_title} >Etudiant en informatique</h5>
                <div className={styles.button} ><a href="#about">EN SAVOIR PLUS</a></div>
            </div>
        </div>
    );
};

export default Hero;