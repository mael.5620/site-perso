import React from 'react';
import ContactForm from '../ContactForm/ContactForm';
import styles from './Contact.module.css'

const Contact = ({ contactSubject }) => {
    return (
        <section className={styles.section} id="contact" >
            <h2 className={styles.title} >Me contacter</h2>
            <p className={styles.sub_title} >Pour me contacter, il vous suffit de m'envoyer un message via ce formulaire et je vous répondrais rapidement.</p>
            <ContactForm contactSubject={contactSubject} />
        </section>
    );
};

export default Contact;

